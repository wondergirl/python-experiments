__author__ = 'ann'
import re
from django.utils.html import escape
from django.utils import timezone


def html_to_content(h):
    return escape(h)


def content_to_html(s):
    s = re.sub('\n', '<br>\n', s)
    return s


def increment_counter(request, name_counter):
    if name_counter in request.session:
        request.session[name_counter] += 1
    else:
        request.session[name_counter] = 1

    # set session start if it's not set already
    if not 'start_session' in request.session:
        request.session['start_session'] = timezone.now()

