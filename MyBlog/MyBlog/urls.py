from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', 'MyBlogApp.views.home', name="home"),
                       url(r'^myblog/$', 'MyBlogApp.views.my_blog', name="my_blog"),
                       url(r'^editblog/(?P<id_article>(\d+|$))', 'MyBlogApp.views.edit_article', name="edit_blog"),
                       url(r'^myblog/(?P<id_article>\d+)', 'MyBlogApp.views.view_article'),
                       url(r'^addblog/$', 'MyBlogApp.views.add_article'),
                       url(r'^deleteblog/(?P<id_article>\d+)', 'MyBlogApp.views.delete_article'),
                       url(r'^clear_session_data/$', 'MyBlogApp.views.clear_session_data'),
                       url(r'^admin/', include(admin.site.urls)),
)
