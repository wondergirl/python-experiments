from django.db import models


class Article(models.Model):
    id_article = models.IntegerField(primary_key=True, null=False)
    name = models.CharField(max_length=50)
    content = models.TextField()
    version = models.IntegerField(default=0)
    date = models.DateTimeField(auto_now=True)


    @classmethod
    def get_articles(cls, conditions={}):
        return cls.objects.all()

    @classmethod
    def get_by_id(cls, id_article):
        try:
            article = cls.objects.get(id_article=id_article)
        except Article.DoesNotExist:
            article = None

        return article

    @classmethod
    def exists(cls, id_article):
        return len(cls.objects.filter(id_article=id_article)) > 0

    @classmethod
    def delete(cls, id_article):
        return cls.objects.filter(id_article=id_article).delete()