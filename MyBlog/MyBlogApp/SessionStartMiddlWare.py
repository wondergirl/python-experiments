__author__ = 'ann'
from django.utils import timezone


class SessionStartMiddlWare(object):
    def process_request(self, request):
        return None

    def process_view(self, request, view_func, view_args, view_kwargs):
        if not 'start_session_with_middleware' in request.session:
            request.session['start_session_with_middleware'] = timezone.now()
        return None