__author__ = 'ann'


from rest_framework import serializers
from Yet_Another_Auction_Site_App.models import Auction, Status, Bids
from django.contrib.auth.models import User


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ('id_status', 'title')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'last_name', 'first_name')


class AuctionSerializer(serializers.ModelSerializer):
    status = StatusSerializer()
    user = UserSerializer()

    class Meta:
        model = Auction
        fields = ('id_auction', 'title_auction', 'description',
                  'min_price', 'end_date',
                  'status', 'user')


class BidSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    auction = AuctionSerializer()

    class Meta:
        model = Bids
        fields = ('auction', 'date', 'price',
                  'user')

