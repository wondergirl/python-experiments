# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Yet_Another_Auction_Site_App', '0005_auto_20141101_1020'),
    ]

    operations = [
        migrations.CreateModel(
            name='Winners',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('auction', models.ForeignKey(to='Yet_Another_Auction_Site_App.Auction')),
                ('bid', models.ForeignKey(to='Yet_Another_Auction_Site_App.Bids')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='winners',
            unique_together=set([('auction', 'bid')]),
        ),
        migrations.RemoveField(
            model_name='auction',
            name='winner',
        ),
    ]
