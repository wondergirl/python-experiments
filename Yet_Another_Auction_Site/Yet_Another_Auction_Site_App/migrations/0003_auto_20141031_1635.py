# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Yet_Another_Auction_Site_App', '0002_auto_20141031_1626'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bids',
            options={'ordering': ['date', 'price']},
        ),
    ]
