# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Yet_Another_Auction_Site_App', '0003_auto_20141031_1635'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bids',
            options={'ordering': ['date']},
        ),
        migrations.RenameField(
            model_name='auction',
            old_name='id_status',
            new_name='status',
        ),
        migrations.RenameField(
            model_name='auction',
            old_name='id_user',
            new_name='user',
        ),
        migrations.RenameField(
            model_name='bids',
            old_name='id_auction',
            new_name='auction',
        ),
        migrations.RenameField(
            model_name='bids',
            old_name='id_user',
            new_name='user',
        ),
    ]
