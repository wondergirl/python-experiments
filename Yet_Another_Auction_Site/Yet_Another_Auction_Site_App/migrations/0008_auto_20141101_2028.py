# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Yet_Another_Auction_Site_App', '0007_userdetails'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userdetails',
            old_name='lang',
            new_name='language',
        ),
    ]
