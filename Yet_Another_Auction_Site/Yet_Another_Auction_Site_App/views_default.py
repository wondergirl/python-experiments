__author__ = 'ann'


from Yet_Another_Auction_Site_App.models import Auction, Bids, Winners
from django.contrib.auth.models import User

from django.shortcuts import render
from autofixture import AutoFixture
from django.http import HttpResponseRedirect
from django.contrib import messages

from django.contrib.auth.decorators import user_passes_test
from Yet_Another_Auction_Site_App.views_helpers import check_lang, html_to_content


def home(request):
    check_lang(request)
    # browse all the auctions
    auctions = Auction.objects.exclude(status_id=2)
    for auction in auctions:
        try:
            if auction.id_winner != 0:
                winner = User.objects.get(id=auction.id_winner)
                auction.winner = winner.username
        except Winners.DoesNotExist:
            auction.winner = None

    return render(request, "base_default.html", {'page_name': "Home page",
                                                 'auctions_list': auctions})

@user_passes_test(lambda u: u.is_superuser)
def populate_db(request):
    fixture = AutoFixture(User)
    fixture.create(5)

    fixture = AutoFixture(Auction)
    fixture.create(50)

    fixture = AutoFixture(Bids)
    fixture.create(5)

    messages.add_message(request, messages.INFO, 'The database was populated with 50 Users, 50 Auctions, 5 Bids')
    return HttpResponseRedirect('/admin')


def translate(request):
    language = html_to_content(request.GET.get('lan'))
    page = html_to_content(request.GET.get('page'))
    request.session["lang"] = language

    return HttpResponseRedirect(page)