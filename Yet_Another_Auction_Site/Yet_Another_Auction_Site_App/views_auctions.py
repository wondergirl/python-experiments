__author__ = 'ann'


from Yet_Another_Auction_Site_App.models import Auction, Bids

from Yet_Another_Auction_Site.forms import CreateAuctionForm

from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
from Yet_Another_Auction_Site_App.views_helpers import html_to_content, is_mine, check_lang
from django.db.models import Max
import decimal
from django.contrib.auth.decorators import user_passes_test
from django.conf import settings
from django.utils.translation import ugettext as _

from datetime import datetime, timedelta
from django.utils.timezone import utc


@login_required
def create_auction(request):
    if request.method == 'POST':
        # verify data
        form = CreateAuctionForm(request.POST)
        if form.is_valid():
            result = form.verify()
            if result.get('type') == "error":
                messages.add_message(request, messages.INFO, result.get('message'))

            # if the No button was pressed
            elif request.POST.get('cancel', '') == "No":
                messages.add_message(request, messages.INFO, 'The creation of auction was cancelled')
                return HttpResponseRedirect(reverse("home"))

            # if the Yes button was pressed
            elif request.POST.get('confirm', '') == "Yes":
                result = form.save(request)
                # send email to the seller
                auction = result.get('object')
                body = "The auction you created was successfully launched! Congratulations!\n" \
                       "Information about the auction:\n" \
                       "- Title: " + auction.title_auction + "\n" \
                       "- Description: " + auction.description + "\n" \
                       "- Starting price: " + str(auction.min_price) + "\n" \
                       "- Deadline: " + str(auction.end_date)
                to_email = request.user.email
                send_mail('The Auction was launched!', body, settings.FROM_EMAIL, [to_email, ], fail_silently=False)

                messages.add_message(request, messages.INFO, result.get('message'))
                return HttpResponseRedirect(reverse("home"))

            # if the form was just completed and sent for confirmation
            elif request.POST.get('confirm', '') == "":
                return render(request, "auction/base_create_auction_form.html", {'page_name': "Confirmation",
                                                                                 'confirm': True,
                                                                                 'form': result.get('form')})

    else:
        form = CreateAuctionForm()

    return render(request, "auction/base_create_auction_form.html", {'form': form, 'page_name': "Create new auction"})


def view_auction(request, id_auction):
    check_lang(request)

    try:
        auction = Auction.objects.get(id_auction=id_auction)
    except Auction.DoesNotExist:
        messages.add_message(request, messages.ERROR, 'The auction does not exist')
        return HttpResponseRedirect(reverse("home"))

    bids = Bids.objects.filter(auction_id=id_auction)
    winning_bid = Bids.objects.filter(auction_id=id_auction).all().aggregate(Max('price'))['price__max']
    request.session["auction_" + id_auction + "_view_v"] = auction.version

    return render(request, "auction/base_view_auction.html", {'page_name': "View an auction",
                                                              'auction': auction,
                                                              'bids': bids, 'winning_bid': winning_bid})


def search_auction(request):
    to_search = html_to_content(request.GET.get('search', ''))
    auctions = Auction.objects.filter(title_auction__contains=to_search).exclude(status_id=2)
    return render(request, "base_default.html", {'page_name': "Home page",
                                                 'auctions_list': auctions,
                                                 'search': to_search})


@user_passes_test(lambda u: u.is_superuser)
def ban_auction(request, id_auction):
    try:
        auction = Auction.objects.get(id_auction=id_auction)
    except Auction.DoesNotExist:
        check_lang(request)
        messages.error(request, _('The auction does not exist'))
        return HttpResponseRedirect(reverse("home"))

    if auction.status_id == 2:
        messages.error(request, _('The auction was already banned'))
        return HttpResponseRedirect(reverse("home"))

    #concurrency - see the last description before bidding
    try:
        print(auction.version)
        print(request.session["auction_" + id_auction + "_view_v"])
        if auction.version != request.session["auction_" + id_auction + "_view_v"]:
            messages.add_message(request, messages.ERROR, 'Description was edited. Please see anpther descriptio then ban!')
            return HttpResponseRedirect(reverse("view_auction", kwargs={'id_auction': id_auction}))
    except KeyError:
        # the link was accessed directly for the first time, i.e the key was not set
        messages.error(request, _('Follow the right link please!'))
        return HttpResponseRedirect(reverse("home"))


    auction.status_id = 2
    auction.save()


    # email details
    body = "The auction you created was banned!\n" \
                           "Information about the auction:\n" \
                           "- Title: " + auction.title_auction + "\n" \
                           "- Description: " + auction.description + "\n" \
                           "- Starting price: " + str(auction.min_price) + "\n" \
                           "- Deadline: " + str(auction.end_date)

    # notify by email seller
    if not send_mail('The Auction was launched!', body, settings.FROM_EMAIL, [auction.user.email, ], fail_silently=False):
        messages.add_message(request, messages.ERROR, 'The email was not send to the seller')

    #notify by email all the bidders
    # get all the bidders
    bids = Bids.objects.filter(auction_id=id_auction)
    if bids:
        body = "The auction you participated in was banned!\n" \
                               "Information about the auction:\n" \
                               "- Title: " + auction.title_auction + "\n" \
                               "- Description: " + auction.description + "\n" \
                               "- Starting price: " + str(auction.min_price) + "\n" \
                               "- Deadline: " + str(auction.end_date)
        for bid in bids:
            if not send_mail('The Auction was banned!', body, settings.FROM_EMAIL, [bid.user.email, ], fail_silently=False):
                messages.add_message(request, messages.ERROR, 'The email was not send to the bidder: ' + bid.user.username +
                                    ' with email: ' + bid.user.email)
    check_lang(request)
    messages.success(request, _('The auction %(title)s was banned') % {'title':auction.title_auction})
    return HttpResponseRedirect(reverse("home"))

@login_required
def edit_auction(request, id_auction):
    try:
        auction = Auction.objects.get(id_auction=id_auction)
    except Auction.DoesNotExist:
        messages.add_message(request, messages.ERROR, 'This auction does not exist')
        return HttpResponseRedirect(reverse("home"))

    if not is_mine(auction.user_id, request):
        messages.add_message(request, messages.ERROR, 'You cannot edit this auction')
        return HttpResponseRedirect(reverse("home"))

    if not auction.status_id == 1:
        messages.add_message(request, messages.ERROR, 'You cannot edit this auction because it is not active')
        return HttpResponseRedirect(reverse("home"))

    if request.method == 'POST':
        description = html_to_content(request.POST.get('description', ''))
        if description == "":
            messages.add_message(request, messages.ERROR, 'Please introduce description')
            return render(request, "auction/base_edit_auction.html", {'page_name': "Edit auction",
                                                                      'auction': auction})
        auction.description = description
        auction.version += 1
        auction.save()
        print("changed" + str(auction.version))
        messages.add_message(request, messages.INFO, 'Description was edited')
        return HttpResponseRedirect(reverse("view_auction", kwargs={'id_auction': id_auction}))

    else:
        return render(request, "auction/base_edit_auction.html", {'page_name': "Edit auction",
                                                                   'auction': auction})


@login_required
def bid_auction(request, id_auction):
    check_lang(request)
    try:
        auction = Auction.objects.get(id_auction=id_auction)
    except Auction.DoesNotExist:
        messages.error(request, _('This auction does not exist'))
        return HttpResponseRedirect(reverse("home"))

    if auction.id_auction == id_auction:
        messages.error(request, _('You cannot bid this auction because it is yours'))
        return HttpResponseRedirect(reverse("home"))

    if not auction.status_id == 1:
        messages.error(request, _('You cannot bid this auction because it is not active'))
        return HttpResponseRedirect(reverse("home"))

    winning_bid = Bids.objects.filter(auction_id=id_auction).all().aggregate(Max('price'))['price__max']
    current_winner = Bids.get_winner(id_auction)

    if request.method == 'POST':
        # verify description version
        print("id-" + str(request.session["auction_" + id_auction + "_v"]))
        if not auction.version == request.session["auction_" + id_auction + "_v"]:
            messages.error(request, _('You cannot bid this auction because the description has changed. Please check'
                                      ' it again. And then bid'))
            return HttpResponseRedirect(reverse("view_auction", kwargs={'id_auction': id_auction}))

        # verify the bid amount
        try:
            bid_price = decimal.Decimal(html_to_content(request.POST.get('bid_price', '')))
            bid_price = round(bid_price, 2)
            print(bid_price)
        except:
            messages.error(request, _('Introduce the right price'))
            return render(request, "auction/base_bid_form.html", {'page_name': "Bid auction",
                                                           'auction': auction,
                                                           'winning_bid': winning_bid})

        if not bid_price >= 0.01:
            messages.error(request, _('The minimum bid is 0.01'))
            return render(request, "auction/base_bid_form.html", {'page_name': "Bid auction",
                                                           'auction': auction,
                                                           'bid_price': bid_price,
                                                           'winning_bid': winning_bid})

        # verify if the user is already winning the auction
        if current_winner:
            if current_winner.user.id == request.user.id:
                messages.error(request, _('You cannot bid this auction because it you already winning it'))
                return HttpResponseRedirect(reverse("view_auction", kwargs={'id_auction': id_auction}))

        # verify if the bid is greater than the previous and the minimum price
        if float(bid_price) < auction.min_price:
            messages.error(request, _('Bid is less than the minimum price'))
            return render(request, "auction/base_bid_form.html", {'page_name': "Bid auction",
                                                               'auction': auction,
                                                               'bid_price': bid_price,
                                                               'winning_bid': winning_bid})

        # get previous prices
        prev_bids = Bids.objects.filter(auction_id=id_auction)
        for prev_bid in prev_bids:
            print(bid_price)
            if float(bid_price) <= prev_bid.price:
                messages.error(request, _('Bid is less than the previous bids'))
                return render(request, "auction/base_bid_form.html", {'page_name': "Bid auction",
                                                                   'auction': auction,
                                                                   'bid_price': bid_price,
                                                                   'winning_bid': winning_bid})
            print("prev_price" + str(prev_bid.price))

        #verify concurrency
        if current_winner.id_bid != request.session["auction_" + id_auction + "_id_winner"]:
            messages.error(request, _('You cannot bid this auction because the winning bid has changed. Please check'
                                      ' it again. And then bid'))
            return HttpResponseRedirect(reverse("view_auction", kwargs={'id_auction': id_auction}))


        # bid
        data = {'id_auction': id_auction, 'bid_price': bid_price,
                'id_user': request.user.id}
        bid = Bids()
        if not bid.place_bid(data):
            messages.error(request, _('Sorry. An error occurred'))
            return render(request, "auction/base_bid_form.html", {'page_name': "Bid auction",
                                                               'auction': auction,
                                                               'bid_price': bid_price,
                                                               'winning_bid': winning_bid})
        # notify
        # email details
        body = "The auction you created was bidded!\n" \
                               "Information about the auction:\n" \
                               "- Title: " + auction.title_auction + "\n" \
                               "- Description: " + auction.description + "\n" \
                               "- Starting price: " + str(auction.min_price) + "\n" \
                               "- Deadline: " + str(auction.end_date)

        # notify by email seller
        if not send_mail('The Auction was bidded!', body, settings.FROM_EMAIL, [auction.user.email, ], fail_silently=False):
            messages.error(request, _('The email was not send to the seller'))

        #notify by email all the bidders
        # get all the bidders including the last one
        bids = Bids.objects.filter(auction_id=id_auction)
        body = "The auction you participated in was bidded!\n" \
                               "Information about the auction:\n" \
                               "- Title: " + auction.title_auction + "\n" \
                               "- Description: " + auction.description + "\n" \
                               "- Starting price: " + str(auction.min_price) + "\n" \
                               "- Deadline: " + str(auction.end_date)
        for bid in bids:
            if not send_mail('The Auction was bidded!', body, settings.FROM_EMAIL, [bid.user.email, ], fail_silently=False):
                messages.add_message(request, messages.ERROR, 'The email was not send to the bidder: ' + bid.user.username +
                                    ' with email: ' + bid.user.email)

        # soft deadline
        current_deadline = auction.end_date
        date_diff = current_deadline - datetime.utcnow().replace(tzinfo=utc)
        # 5 min= 300 sec
        if date_diff.total_seconds() <= 300 and date_diff.total_seconds() > 0:
            current_deadline += timedelta(seconds=300)
            auction.end_date = current_deadline
            auction.save()
            messages.success(request, _('The deadline was postponed!'))

        # bid was registered
        messages.success(request, _('You placed a bid successfully. Thank you!'))

        return HttpResponseRedirect(reverse("view_auction", kwargs={'id_auction': id_auction}))

    else:
        request.session["auction_" + id_auction + "_v"] = auction.version
        if current_winner:
            request.session["auction_" + id_auction + "_id_winner"] = current_winner.id_bid

        return render(request, "auction/base_bid_form.html", {'page_name': "Bid auction",
                                                              'auction': auction,
                                                              'winning_bid': winning_bid})
