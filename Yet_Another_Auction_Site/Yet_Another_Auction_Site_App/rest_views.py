__author__ = 'ann'


from django.views.decorators.csrf import csrf_exempt
from Yet_Another_Auction_Site_App.serializers import AuctionSerializer, BidSerializer
from Yet_Another_Auction_Site_App.views_helpers import html_to_content
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from Yet_Another_Auction_Site_App.models import Auction, Bids

from django.http import HttpResponse
from django.contrib.auth import authenticate
import base64
import decimal


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def auction_list(request):
    #get credentials
    # username should include salt, encoded in base64 as username:password    correct combination = YWRtaW46MTIz
    temp = base64.decodestring(request.META.get('HTTP_CREDENTIALS'))
    u, p = temp.split(":")

    user = authenticate(username=u, password=p)
    if not user:
        return HttpResponse(status=404)

    if request.method == 'GET':
        auction = Auction.objects.all()
        serializer = AuctionSerializer(auction, many=True)

        return JSONResponse(serializer.data)
    else:
        return HttpResponse(status=404)


@csrf_exempt
def api_search_auction(request, search_value):
    #get credentials
    # username should include salt, encoded in base64 as username:password  YWRtaW46MTIz
    temp = base64.decodestring(request.META.get('HTTP_CREDENTIALS'))
    u, p = temp.split(":")

    user = authenticate(username=u, password=p)
    if not user:
        return HttpResponse(status=404)

    if request.method == 'GET':
        search_value = html_to_content(search_value)
        if search_value == "":
            return HttpResponse(status=404)

        auction = Auction.objects.filter(title_auction__contains=search_value)
        serializer = AuctionSerializer(auction, many=True)

        return JSONResponse(serializer.data)
    else:
        return HttpResponse(status=404)

@csrf_exempt
def api_bid_auction(request, id_auction):
    #get credentials
    # username should include salt, encoded in base64 as username:password  YWRtaW46MTIz
    temp = base64.decodestring(request.META.get('HTTP_CREDENTIALS'))
    u, p = temp.split(":")

    if request.method == "POST":
         # authenticate user
        user = authenticate(username=u, password=p)
        if not user:
            return HttpResponse(status=404)

        #if the user is not a seller
        try:
            auction = Auction.objects.get(id_auction=id_auction)
        except Auction.DoesNotExist:
            return HttpResponse(status=404)

        if auction.user_id == user.id:
            error = "You are not able to bid"

        # if the auction is not active
        if auction.status_id != 1:
            error = "YThe auction is not active"

        # get data
        data = JSONParser().parse(request)
        try:
            bid_price = decimal.Decimal(html_to_content(data.get('price')))
            bid_price = round(bid_price, 2)
            print(bid_price)
        except:
            error = "Introduce the right bid"
            return JSONResponse({"error": error}, status=400)

        id_user = html_to_content(data.get('id_user'))


        current_winner = Bids.get_winner(id_auction)
        if bid_price < 0.01:
            error = "The bid is lees than 0.01"
            return JSONResponse({"error": error}, status=400)

        if float(bid_price) < auction.min_price:
            error = "Bid is less than the minimum price"
            return JSONResponse({"error": error}, status=400)

        print("wiin" + str(current_winner.user.id))
        if int(current_winner.user.id) == int(id_user):
            error = "You cannot bid this auction because it you already winning it"
            return JSONResponse({"error": error}, status=400)

        # get previous prices
        prev_bids = Bids.objects.filter(auction_id=id_auction)
        for prev_bid in prev_bids:
            if float(bid_price) <= prev_bid.price:
                error = "Bid is less than the previous bids"
                return JSONResponse({"error": error}, status=400)

        # bid
        data = {'id_auction': id_auction, 'bid_price': bid_price,
                'id_user': id_user}
        bid = Bids()

        if not bid.place_bid(data):
            error = "Sorry. An error occurred"
            return JSONResponse({"error": error}, status=400)


        bid = Bids.objects.latest('id_bid')
        print(bid.id_bid)
        serializer = BidSerializer(bid)

        return JSONResponse(serializer.data)