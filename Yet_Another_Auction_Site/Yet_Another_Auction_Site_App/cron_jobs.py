__author__ = 'ann'

from django_cron import CronJobBase, Schedule
from Yet_Another_Auction_Site_App.models import Auction, Bids, Winners
from datetime import datetime
from django.core.mail import send_mail
from django.conf import settings



class MyCronJob(CronJobBase):
    RUN_EVERY_MINS = 0.2

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'Yet_Another_Auction_Site_App.my_cron_job'    # a unique code

    def do(self):
        current_date = datetime.today()
        print(current_date)
        # select auctions
        auctions = Auction.objects.filter(status_id=1, end_date__lte=current_date)
        for auction in auctions:
            winner = Bids.get_winner(auction.id_auction)
            if winner is not None:
                print ("- Auction: " + str(auction.id_auction) + " Winner: " + str(winner.user.id) +
                           "Username: " + str(winner.user.username))

                Auction.objects.filter(id_auction=auction.id_auction).update(id_winner=winner.user.id)
                # notify
                # email details
                body = "The auction you created was resolved!\n" \
                                       "Information about the auction:\n" \
                                       "- Title: " + auction.title_auction + "\n" \
                                       "- Description: " + auction.description + "\n" \
                                       "- Starting price: " + str(auction.min_price) + "\n" \
                                       "- Deadline: " + str(auction.end_date)

                # notify by email seller
                send_mail('The Auction was bidded!', body, settings.FROM_EMAIL, [auction.user.email, ], fail_silently=False)
                #notify by email all the bidders
                # get all the bidders including the last one
                bids = Bids.objects.filter(auction_id=auction.id_auction)
                for bid in bids:
                    send_mail('The Auction was bidded!', body, settings.FROM_EMAIL, [bid.user.email, ], fail_silently=False)

        #resolve
        Auction.objects.filter(status_id=1, end_date__lte=current_date).update(status_id=3)