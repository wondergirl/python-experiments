__author__ = 'ann'

"""
    ------------------  UPDATED VERSION  -----------------------------------------------------------
    ------------------  which excludes the use of global variable connection------------------------
    ------------------  and allows not to take care of inputfile and outputfile in friday_webapp----

    Creating a simple web-server with few possibilities:
    1) http://127.0.0.1:8080/ddmmyyyy  - check if the ddmmyyy is Friday
    2) http://127.0.0.1:8080/dateform  - introduce date and password in the form. If the password is correct get
                                         the answer whether the date is Friday. Password is hard-coded!
    3) http://127.0.0.1:8080/log       - get the log of unsuccessful attempts to submit the form
"""

import socket
import re
from datetime import datetime


PORT = 8080
HOST = "127.0.0.1"
FILE_NAME = "log.txt"


# -------  Start Service functions --------- #
def is_it_friday(d):
    print(d)
    if d.isoweekday() == 5:
        return "Yes, it is Friday!"
    else:
        return "Nope."


def get_received_params(params_str):
    data_final = {}

    data_received = params_str.split('&')
    for param in data_received:
        temp = param.split('=')
        data_final[str(temp[0])] = str(temp[1])

    return data_final


def is_int(str):
    try:
        x = int(str)
    except ValueError:
        return False

    return True
# -------  End Service functions --------- #


class ProcessRequestClass(object):
    # valid addresses http://127.0.0.1:8080/ddmmyyyy or http://127.0.0.1:8080/dateform or http://127.0.0.1:8080/log
    url_pattern = "/(?P<url>([0-2][0-9]|[3][0-1])([0][0-9]|[1][0-2])(\d{4})|dateform|log)"

    def __init__(self, connection, handler):
        self.connection = connection
        self.handler = handler

    def run(self):
        self.inputfile = self.connection.makefile('rb', -1)
        self.outputfile = self.connection.makefile('wb', 0)

        request = self.read_request()
        # result_request = self.parse_request(request)

        return self.handler(self.parse_request(request))

    def close(self):
        self.inputfile.close()
        self.outputfile.close()
        self.connection.close()
        print "Connection closed."

    def parse_post_data(self):
        self.connection.shutdown(socket.SHUT_RD)

        for line in self.inputfile:
            if len(line.strip().replace('\r', '').replace("\n", "")) == 0:    # Found the empty line in the request headers
                break

        #data_line = self.inputfile.readline()
        #data_line = ProcessRequestClass.read_request(self)
        return self.read_request()

    def read_request(self):
        return self.inputfile.readline()

    def send_response(self, s):
        self.outputfile.write(self.create_response(s))


    """
        Params -
            mess_text = "Bad Request pal!"
            mess_type = "error"/"success"
            mess_title = "Error"
    """
    def create_response(self, params):
        mess_text = "Bad Request pal!"
        mess_type = "error"
        mess_title = "Error"
        mess_flag = "400 Bad Request"

        if not params.get('mess_title') is None:
            mess_title = str(params.get('mess_title'))

        if not params.get('mess_text') is None:
            mess_text = str(params.get('mess_text'))

        if not mess_type == "error":
            mess_flag = "200 OK"

        return "HTTP/1.1 " + mess_flag + "\n\r" + create_document(mess_text, mess_title) + "\n\r"

    def parse_request(self, request):
        lines = request.split('\n\r')
        if len(lines) < 1:
            return False
        words = lines[0].split()

        if len(words) < 3:
            return False

        match_res = re.match(ProcessRequestClass.url_pattern, words[1])

        if (words[0] == "GET" or words[0] == "POST") and \
                not match_res is None and \
                words[2] in ["HTTP/1.0", "HTTP/1.1"]:
            method = words[0]
            url = match_res.group('url')

            if words[0] == "POST":
                post_data = self.parse_post_data()
                return (method, url, post_data)
            else:
                return (method, url, [])
        else:
            return None


def server(handler_func, handler_class=ProcessRequestClass, port=PORT, host=HOST, queue_size=5):
    mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mysocket.bind((host, port))
    mysocket.listen(queue_size)

    while True:
        print "Waiting at http://%s:%d" % (host, port)
        (connection, addr) = mysocket.accept()
        print "New connection", connection, addr

        handler = handler_class(connection, handler_func)   #all connection processing are hide in handlerClass
        handler.send_response(handler.run())
        handler.close()


def create_document(s, title="Web app"):
    return "Content-Type: text/html;\n\r\n\r"+ \
           "<html><body>\n\r<h3>" + title + "</h3>" + s + "</body></html>\n\r"


def friday_webapp(result_request):
    # if the url didn't match the proper ones we show the error message and stop executing the script
    if result_request is None:
        return {"mess_text": "Bad Request pal!"}

    if result_request[1] == "dateform":
        # hard-coded password
        password = "123d"

        if result_request[0] == "POST":
            # unserialize received params
            data_final = get_received_params(result_request[2])

            #if the password was incorrect
            if not data_final.get('passwd') == password:
                # to log
                # if the file doesn't exist, it will be created
                file = open(FILE_NAME, 'a')
                file.write("\n" + str(datetime.now()) + " user entered incorrect password: " + data_final.get('passwd'))
                file.close()
                return {"mess_text": "You have introduced the wrong password"}

            # if the date was not correct
            if not (len(data_final.get('date')) == 8 and is_int(data_final.get('date'))):
                return {"mess_text": "Please introduce the correct date!"}

            # if all the data was correct
            if data_final.get('passwd') == password:
                response = {"mess_text": is_it_friday(datetime.strptime(data_final.get('date'), '%d%m%Y')),
                            "mess_title": "Is " + data_final.get('date') + " Friday?",
                            "mess_type": "success"}
        else:
            response = {"mess_text": form_templ()}
    elif result_request[1] == "log":
        log_file = open(FILE_NAME, "r")
        log_text = log_file.read().replace("\n", "</br>")
        log_file.close()

        response = {"mess_text": log_text,
                    "mess_title": "Access log",
                    "mess_type": "success"}
    elif int(result_request[1]):
        response = {"mess_text": is_it_friday(datetime.strptime(result_request[1], '%d%m%Y')),
                    "mess_title": "Is " + str(result_request[1]) + " Friday?",
                    "mess_type": "success"}

    return response


# form template
def form_templ():
    html = '''
        <p>Form</p>
        <form name="f1" method="post" action="/dateform">
            <label>Date<input name="date" type="text" maxlength="8" required/></label></br>
            <label>Password<input name="passwd" type="password" size="25" maxlength="30" required/></label></br>
            <input type="submit" name="enter" value="Ok" />
        </form>
        '''
    return html


server(friday_webapp)