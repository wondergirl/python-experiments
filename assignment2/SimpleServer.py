__author__ = 'ann'

"""
    Creating a simple web-server with few possibilities:
    1) http://127.0.0.1:8080/ddmmyyyy  - check if the ddmmyyy is Friday
    2) http://127.0.0.1:8080/dateform  - introduce date and password in the form. If the password is correct get
                                         the answer whether the date is Friday. Password is hard-coded!
    3) http://127.0.0.1:8080/log       - get the log of unsuccessful attempts to submit the form
"""

import socket
import re
from datetime import datetime


PORT = 8080
HOST = "127.0.0.1"
FILE_NAME = "log.txt"


# -------  Start Service functions --------- #

def is_it_friday(d):
    print(d)
    if d.isoweekday() == 5:
        return "Yes, it is Friday!"
    else:
        return "Nope."


def is_int(str):
    try:
        x = int(str)
    except ValueError:
        return False

    return True

def get_received_params(params_str):
    data_final = {}

    data_received = params_str.split('&')
    for param in data_received:
        temp = param.split('=')
        data_final[str(temp[0])] = str(temp[1])

    return data_final
# -------  End Service functions --------- #


def server(handler, port=PORT, host=HOST, queue_size=5):
    global connection

    mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mysocket.bind((host, port))
    mysocket.listen(queue_size)
    while True:
        print "Waiting at http://%s:%d" % (host, port)
        (connection, addr) = mysocket.accept()
        print "New connection", connection, addr
        inputfile = connection.makefile('rb', -1)
        outputfile = connection.makefile('wb', 0)
        handler(inputfile, outputfile)
        inputfile.close()
        outputfile.close()
        connection.close()
        print "Connection closed."


def parse_post_data(inputfile):
    global connection

    connection.shutdown(socket.SHUT_RD)

    for line in inputfile:
        if len(line.strip().replace('\r', '').replace("\n", "")) == 0:    # Found the empty line in the request headers
            break

    data_line = inputfile.readline()

    return data_line


def read_request(inputfile):
    return inputfile.readline()


def parse_request(request, inputfile):
    lines = request.split('\n\r')
    if len(lines) < 1:
        return False
    words = lines[0].split()

    if len(words) < 3:
        return False

    # valid addresses http://127.0.0.1:8080/ddmmyyyy or http://127.0.0.1:8080/dateform or http://127.0.0.1:8080/log
    match_res = re.match("/(?P<url>([0-2][0-9]|[3][0-1])([0][0-9]|[1][0-2])(\d{4})|dateform|log)", words[1])

    if (words[0] == "GET" or words[0] == "POST") and \
            not match_res is None and \
            words[2] in ["HTTP/1.0", "HTTP/1.1"]:
        method = words[0]
        url = match_res.group('url')

        if words[0] == "POST":
            post_data = parse_post_data(inputfile)
            return (method, url, post_data)
        else:
            return (method, url, [])
    else:
        return None


def create_document(s, title="Web app"):
    return "Content-Type: text/html;\n\r\n\r"+ \
           "<html><body>\n\r<h3>" + title + "</h3>" + s + "</body></html>\n\r"


def create_response(status,s):
    return "HTTP/1.1 "+status+"\n\r"+ \
           s+"\n\r"


def friday_webapp(inputfile,outputfile):
    request = read_request(inputfile)
    result_request = parse_request(request, inputfile)

    # if the url didn't match the proper ones we show the error message and stop executing the script
    if result_request is None:
        response = create_response(
            "400 Bad Request",
            create_document("Bad Request pal!", "Error")
            )
        send_response(outputfile, response)
        return

    if result_request[1] == "dateform":
        # hard-coded password
        password = "123d"

        if result_request[0] == "POST":

            # unserialize received variables

            data_final = get_received_params(result_request[2])

            #if the password was incorrect
            if not data_final.get('passwd') == password:
                # to log
                # if the file doesn't exist, it will be created
                file = open(FILE_NAME, 'a')
                file.write("\n" + str(datetime.now()) + " user entered incorrect password: " + data_final.get('passwd'))
                file.close()
                response = create_response(
                    "200 OK",
                    create_document("You have introduced the wrong password", "Error")
                    )
                send_response(outputfile, response)
                return

            # if the date was not correct
            if not (len(data_final.get('date')) == 8 and is_int(data_final.get('date'))):
                 response = create_response(
                            "400 Bad Request",
                            create_document("Please introduce the date!", "Error")
                 )
                 send_response(outputfile, response)
                 return

            # if all the data was correct
            if data_final.get('passwd') == password:
                response = create_response(
                    "200 OK",
                    create_document(is_it_friday(datetime.strptime(data_final.get('date'), '%d%m%Y')), "Is " + data_final.get('date') + " Friday?")
                    )
        else:
            response = create_response(
                "200 OK",
                create_document(form_templ())
                )
    elif result_request[1] == "log":
        log_file = open(FILE_NAME, "r")
        log_text = log_file.read().replace("\n", "</br>")
        log_file.close()

        response = create_response(
            "200 OK",
            create_document(log_text, "Access log")
             )
    elif int(result_request[1]):
        response = create_response(
            "200 OK",
            create_document(is_it_friday(datetime.strptime(result_request[1], '%d%m%Y')), "Is " + str(result_request[1]) + " Friday?")
            )

    send_response(outputfile, response)


def send_response(outputfile, s):
    outputfile.write(s)


# form template
def form_templ():
    html = '''
        <p>Form</p>
        <form name="f1" method="post" action="/dateform">
            <label>Date<input name="date" type="text" maxlength="8" required/></label></br>
            <label>Password<input name="passwd" type="password" size="25" maxlength="30" required/></label></br>
            <input type="submit" name="enter" value="Ok" />
        </form>
        '''
    return html


server(friday_webapp)