from django.db import models

class Article(models.Model):
    name = models.CharField(max_length=30, primary_key=True)
    content = models.TextField()

    @classmethod
    def getByName(cls, name):
        return cls.objects.get(name=name)

    @classmethod
    def exists(cls, name):
        return len(cls.objects.filter(name=name)) > 0
