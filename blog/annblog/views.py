from models import Article
from django.shortcuts import get_object_or_404, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.template import Template, Context, RequestContext
from django.template.loader import get_template
import re,string
from blogfy import content_to_html, html_to_content


def show_article(request, name):
    if Article.exists(name):
        # the article already exists, we show it
        article = Article.getByName(name)

        # Or you can use following statement to get the article without using class method
        #article = Article.objects.get(name=name)
    else:
        # the article does not exists, we let the user create it
        return edit_article(request, name)

    # convert {{WikiWords}} to HTML links
    content_in_html = content_to_html(article.content)

    # load template from a file
    t = get_template("show.html")
    # merge the template with the data from the article
    html = t.render(Context({'name': article.name, 'content': content_in_html}))
    # create a response object and return it
    return HttpResponse(html)


def show_home(request):
    # / points to the "Home" article
    return show_article(request, "Home")

def edit_article(request, name):
    if Article.exists(name):
        # find an existing article
        article = Article.getByName(name)
    else:
        # create a new article
        article = Article(name)

    if request.method == "POST" and request.POST.has_key("content"):
        # a valid POST request: save the new contents of the article
        # Always clean the input from the user
        article.content = html_to_content(request.POST["content"])
        article.save()
        # Always redirect after a successful POST request
        return HttpResponseRedirect('/wiki/' + article.name)
    else:
        # a GET request or a POST request using the worng form: show the form
        return render_to_response("edit.html",
                                  {'name': article.name, 'content': article.content},
                                  context_instance=RequestContext(request)
                                  )
