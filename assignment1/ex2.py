__author__ = 'Anna Corobco'


def str_to_int_or_float(number):
    try:
        return int(number)
    except ValueError:
        return float(number)


class Number(object):

    def __init__(self, numbers):
        self.numbers = numbers

    @classmethod
    def init_with_string(cls, str_nr):
        import re

        pattern = "\d+"
        nrs = re.findall(pattern, str_nr)

        # convert the strings into int or float using list comprehension
        nrs = [str_to_int_or_float(y) for y in nrs]

        return cls(nrs)

    def get_numbers(self):
        return self.numbers

    def average(self):
        import math

        if len(self.numbers) == 0:
            return None

        return math.fsum(self.numbers) / len(self.numbers)

# -----------------------------------------TESTING---------------------------------------------------------------
# A - more than 12
# B - from 6-12
# C - less than 6
results = {'A': [], 'B': [], 'C': []}

choice = 1

while choice == 1:

    numbers_given = input("Please introduce the string.\n")

    x = Number.init_with_string(numbers_given)

    avg = x.average()
    if not avg is None:
        if avg < 6:
            results['C'].append("The average for the numbers " + str(x.get_numbers()) + " - " + str(avg) + " is low")
        elif avg > 12:
            results['A'].append("The average for the numbers" + str(x.get_numbers()) + " - " + str(avg) + " is high")
        else:
            results['B'].append("The average for the numbers" + str(x.get_numbers()) + " - " + str(avg) + " is medium")

    choice = int(input("!!!!!!!!!!!!! \n Please introduce 1 to start introducing another set of numbers "
                           "or another number to stop introducing and to get the results \n"))

# print the collected results
for key in results:
    if results[key]:
        print "-----" + key + ": "
        for result in results[key]:
            print result