__author__ = 'Anna Corobco'


class Number(object):
    def __init__(self, numbers):

        # verify if the elements in the list are int or float
        # if not remove them from the list
        for number in numbers:
            if not (isinstance(number, int) or isinstance(number, float)):
                numbers.remove(number)

        self.numbers = numbers

    def get_numbers(self):
        return self.numbers

    def average(self):
        import math

        if len(self.numbers) == 0:
            return None

        return math.fsum(self.numbers) / len(self.numbers)

# -----------------------------------------TESTING---------------------------------------------------------------
# A - more than 12
# B - from 6-12
# C - less than 6
results = {'A': [], 'B': [], 'C': []}

choice = 1

while choice == 1:

    numbers_given = []

    number = input("Please introduce the number and press Enter. If you want to stop introducing them, press 0 \n")

    while number:
        numbers_given.append(number)
        number = input("Please introduce the the number. If you want to stop introducing them, press 0 \n")

    x = Number(numbers_given)

    avg = x.average()
    if not avg is None:
        if avg < 6:
            results['C'].append("The average for the numbers " + str(x.get_numbers()) + " - " + str(avg) + " is low")
        elif avg > 12:
            results['A'].append("The average for the numbers" + str(x.get_numbers()) + " - " + str(avg) + " is high")
        else:
            results['B'].append("The average for the numbers" + str(x.get_numbers()) + " - " + str(avg) + " is medium")

    choice = int(input("!!!!!!!!!!!!! \n Please introduce 1 to start introducing another set of numbers "
                       "or another number to stop introducing and to get the results \n"))

# print the collected results
for key in results:
    if results[key]:
        print "-----" + key + ": "
        for result in results[key]:
            print result
