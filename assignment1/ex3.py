__author__ = 'Anna Corobco'


def from_now_days(d1):
    from datetime import datetime

    d1 = datetime.strptime(d1, "%d-%m-%Y")
    diff = (datetime.now() - d1).days

    if diff < 0:
        return "-"
    else:
        return diff


class Article(object):
    def __init__(self, name, date, stored):
        self.name = name
        self.date = date
        self.stored = stored

    @classmethod
    def init_process(cls, article_name):
        temp = article_name.split('/')

        # if the array hasn't 5 elements, i.e. if the string wasn't like this one "articles/2014/9/12/my_summer"
        # we return None
        if len(temp) != 5:
            return None

        date_list = temp[1:4]
        date_list.reverse()
        date = '-'.join(date_list)
        return cls(temp[4], date, from_now_days(date))

    def get_name(self):
        return self.name

    def get_date(self):
        return self.date

    def get_stored(self):
        return self.stored

    # method which receives all articles at once as an array and return the whole information about them
    @staticmethod
    def get_full_info_multiple(articles_array):
        result = []

        for article in articles_array:
            temp = article.split('/')

            # if the array hasn't 5 elements, i.e. if the string wasn't like this one "articles/2014/9/12/my_summer"
            # we return None
            if len(temp) != 5:
                return None

            date_list = temp[1:4]
            date_list.reverse()
            date = '-'.join(date_list)
            result.append({'name': temp[4], 'date': date, 'stored': from_now_days(date)})
        #result.append({'name': self.get_name(), 'date': self.get_date(), 'stored': self.get_stored()})

        return result

    # method which receives just one article and returns the information about it
    def get_full_info_one(self):
        result = {'name': self.get_name(), 'date': self.get_date(), 'stored': self.get_stored()}

        return result


#----------------------------------------------TESTING ------------------------------------------------------
choice_type = 1 # introduce all articles at once

while choice_type != 3:
    if choice_type == 1:
        choice = 1
        articles_list = []

        while choice == 1:
            article_introduced = input("Please introduce the path.\n")
            articles_list.append(article_introduced)

            choice = int(input("!!!!!!!!!!!!! \n Please introduce 1 to introduce another path "
                               "or another number to stop introducing and to get the results \n"))

         #articles_list = Article(["articles/2014/9/12/my_summer", "articles/2010/12/21/my_winter"])
        articles_info = Article.get_full_info_multiple(articles_list)
    elif choice_type == 2:
        choice = 1
        articles_info = []
        while choice == 1:
            article_introduced = input("Please introduce the path.\n")
            articles = Article.init_process(article_introduced)

            if not articles is None:
                 articles_info.append(articles.get_full_info_one())

            choice = int(input("!!!!!!!!!!!!! \n Please introduce 1 to introduce another path "
                               "or another number to stop introducing and to get the results \n"))

    # if the result was obtained
    if not articles_info is None:
        for article in articles_info:
                print "Name: " + article['name'] + "\nDate: " + article['date'] + "\nStored for " + str(article['stored']) + \
                      " day(s)\n\t\t****\n"

    choice_type = int(input("!!!!!!!!!!!!! \n Please introduce 1 to use the 1st method of input "
                           "or 2 to use the 2nd method of input or 3 to stop the application\n"))






