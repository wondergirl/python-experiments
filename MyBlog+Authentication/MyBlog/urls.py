from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', 'MyBlogApp.views.home', name="home"),
                       url(r'^myblog/$', 'MyBlogApp.views.my_blog', name="my_blog"),
                       url(r'^editblog/(?P<id_article>(\d+))', 'MyBlogApp.views.edit_article', name="edit_blog"),
                       url(r'^myblog/(?P<id_article>\d+)', 'MyBlogApp.views.view_article'),
                       url(r'^addblog/$', 'MyBlogApp.views.add_article'),
                       url(r'^deleteblog/(?P<id_article>\d+)', 'MyBlogApp.views.delete_article'),
                       url(r'^clear_session_data/$', 'MyBlogApp.views.clear_session_data'),
                       url(r'^createuser/$', 'MyBlogApp.views.create_user'),
                       url(r'^login/$', 'MyBlogApp.views.login_view'),
                       url(r'^logout/$', 'MyBlogApp.views.logout_view'),

                       url(r'^api/v1/add_blog/$', 'MyBlogApp.rest_views.add_blog'),

                       url(r'^admin/', include(admin.site.urls)),
)
