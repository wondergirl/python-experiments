__author__ = 'ann'

from django.forms import widgets
from rest_framework import serializers
from MyBlogApp.models import Article


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ('id_article', 'name', 'content')
