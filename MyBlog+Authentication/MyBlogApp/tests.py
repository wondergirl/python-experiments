from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class EditBlogTest(TestCase):
    fixtures = ['data.json']

    def setUp(self):
        self.existing_blog_id = 1

        self.client = Client()
        self.username = "anna"
        self.email = "ana@utu.fi"
        self.password = "test"
        self.test_user = User.objects.create_user(self.username, self.email, self.password)
        login = self.client.login(username=self.username, password=self.password)
        self.assertEqual(login, True)

    # page access tests
    def test_access(self):
        resp = self.client.get('/editblog')
        self.assertEqual(resp.status_code, 404)

    def test_not_existing_access(self):
        resp = self.client.get('/editblog/9000000000')
        self.assertEqual(resp.status_code, 200)

    def test_alpha_access(self):
        resp = self.client.get('/editblog/dsd-ew')
        self.assertEqual(resp.status_code, 404)

    def test_alpha_num_access(self):
        resp = self.client.get('/editblog/ds32')
        self.assertEqual(resp.status_code, 404)

    def test_update_blog(self):
        session = self.client.session

        # the blog was not updated by the other person
        session['current_article_version'] = 1
        session.save()
        # positive testing
        resp = self.client.post('/editblog/' + str(self.existing_blog_id),
                                {'content': 'new content', 'name': 'new title'})
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, reverse("home"))
        #verify if the blog has been successfully changed
        edited_blog = self.client.get('/editblog/' + str(self.existing_blog_id))
        print "Modified: \n" + str(edited_blog)

        # Negative testing
        # 1 - Data validation
        resp = self.client.post('/editblog/' + str(self.existing_blog_id),
                                {'content': '           <script>alert("Hello!)</script>\'OR 1=1\'', 'name': '<?php echo "anna"; ?>'})
        self.assertEqual(resp.status_code, 302)
        edited_blog = self.client.get('/editblog/' + str(self.existing_blog_id))
        print "Modified: \n" + str(edited_blog)

        # 2 - not all data provided  - just content
        resp = self.client.post('/editblog/' + str(self.existing_blog_id),
                                {'content': '           <script>alert("Hello!)</script>\'OR 1=1\'', 'name': ''})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("form", resp.content)

        # 3 - not all data provided  - just name
        resp = self.client.post('/editblog/' + str(self.existing_blog_id),
                                {'content': '', 'name': '           <script>alert("Hello!)</script>\'OR 1=1\''})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("form", resp.content)

        # 2 - no data provided at all
        resp = self.client.post('/editblog/' + str(self.existing_blog_id), {'content': '', 'name': ''})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("form", resp.content)

        # the blog was updated by the other person
        session['current_article_version'] = 0
        session.save()
        resp = self.client.post('/editblog/' + str(self.existing_blog_id),
                                {'content': 'new content', 'name': 'new title'})
        self.assertTemplateUsed(resp, "base_conflict.html")

    def test_auth_access(self):
        resp = self.client.get('/editblog/' + str(self.existing_blog_id))
        self.assertEqual(resp.status_code, 200)
        self.assertIn("form", resp.content)

    def test_not_auth_access(self):
        self.client.logout()
        resp = self.client.get('/editblog/' + str(self.existing_blog_id))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/login/?next=/editblog/' + str(self.existing_blog_id))
