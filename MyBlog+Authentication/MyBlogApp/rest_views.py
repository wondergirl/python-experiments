__author__ = 'ann'

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from MyBlogApp.serializers import ArticleSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


# {"name": "eeee", "content": "cooontent"} - example of body request
@csrf_exempt
def add_blog(request):
    if request.method == "PUT":
        data = JSONParser().parse(request)
        serializer = ArticleSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        else:
            return JSONResponse(serializer.errors, status=400)
