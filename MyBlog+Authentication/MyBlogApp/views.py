from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect
from MyBlog.MyBlogFy import content_to_html, html_to_content, increment_counter
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.contrib import messages


from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password


from models import Article


def home(request):
    articles = Article.get_articles(request)
    for article in articles:
        article.content = content_to_html(article.content)

    return render(request, "base_home.html", {'articles_list': articles, 'page_name': "Home Page"})


def my_blog(request):
    articles = Article.get_articles(request)
    for article in articles:
        article.content = content_to_html(article.content)

    return render(request, "base_articles.html", {'articles_list': articles, 'page_name': "My Blog"})


def show_edit_form_article(request, id_article):
    if not Article.exists(id_article):
        return render(request, "error.html",
                      {"error_mess": "Such article does not exist. So you cannot edit it"},
                      context_instance=RequestContext(request))

        # or we can use messages and then redirect user to the main page
        #messages.add_message(request, messages.INFO, 'Such article does not exist')
        # return HttpResponseRedirect(reverse("home"))

    # the article already exists, we show it
    article = Article.get_by_id(id_article)

    # record the current version in the session
    request.session["current_article_version"] = article.version

    return render(request, "base_edit_article.html",
                      {"article": article, 'page_name': "Edit Article"},
                      context_instance=RequestContext(request))

@login_required
def edit_article(request, id_article):
    user_edits_article = False

    # if we just view the article
    if request.method == "GET":
        return show_edit_form_article(request, id_article)

    # if we edit the existing article, i.e the parameter id_article has been transmitted
    if id_article and Article.exists(id_article):
        article = Article.get_by_id(id_article)

        # create the flag in order to increase the amount of edited articles after the user submits the form
        user_edits_article = True
    else:

        # else we create a new article and write the version 0 to the session
        article = Article()
        request.session["current_article_version"] = 0

    # verify data received from the form and if there is written article version in the session
    if not request.POST.get("content") == "" and not request.POST.get("name") == "" and 'current_article_version' in request.session:

        # filter data
        name = html_to_content(request.POST["name"])
        content = html_to_content(request.POST["content"])

        if article.version == request.session.get('current_article_version'):
            article.name = name
            article.content = content
            article.version += 1
            article.save()

            # increase the needed value stored in the session
            if user_edits_article:
                increment_counter(request, "articles_edited")
            else:
                increment_counter(request, "articles_created")
                messages.add_message(request, messages.INFO, 'The article was successfully created.')

            return HttpResponseRedirect(reverse("home"))
        else:

            # let the user solve the conflict
            request.session["current_article_version"] = article.version
            return render_to_response("base_conflict.html",
                                      {'article': article,
                                       'user_name': name,
                                       'user_content': content},
                                      context_instance=RequestContext(request)
                                      )
    return render(request, "base_edit_article.html",
                      {"article": article},
                      context_instance=RequestContext(request))


def view_article(request, id_article):
    if not Article.exists(id_article):
        return render(request, "error.html",
                      {"error_mess": "Such article does not exist. So you cannot view it"},
                      context_instance=RequestContext(request))

        # or we can use messages and then redirect user to the main page
        #messages.add_message(request, messages.INFO, 'Such article does not exist')
        # return HttpResponseRedirect(reverse("home"))

    article = Article.get_by_id(id_article)
    article.content = content_to_html(article.content)
    increment_counter(request, "articles_visited")

    return render(request, "base_view_article.html", {"article": article, 'page_name': "View Article"})

@login_required
def add_article(request):
    return render_to_response("base_edit_article.html",
                                           {"article": "sas", 'page_name': "Add article"},
                                           context_instance=RequestContext(request))

@login_required
def delete_article(request, id_article):
    if Article.exists(id_article):
        Article.delete(id_article)
        increment_counter(request, "articles_deleted")
    return HttpResponseRedirect('/')


def clear_session_data(request):
    request.session.clear()
    return HttpResponseRedirect('/')


def create_user(request):
    if request.method == "POST":
        username = html_to_content(request.POST['login_user'])
        email = html_to_content(request.POST['email_user'])

        #print(password)

        # add verification
        user_exists = User.objects.filter(username=username)
        nr_errors = 0
        if user_exists:
            messages.add_message(request, messages.INFO, "User with such username already exists")
            nr_errors += 1

        user_exists = User.objects.filter(email=email)
        if user_exists:
            messages.add_message(request, messages.INFO, "User with such email already exists")
            nr_errors += 1

        if nr_errors > 0:
            return render_to_response("base_create_user_form.html",
                                           {'page_name': "Register new user",
                                            "username": username,
                                            "email": email},
                                           context_instance=RequestContext(request))

        user = User.objects.create_user(username, email)

        # automatically uses hashing algorithm
        user.set_password(html_to_content(request.POST['pswd_user']))
        user.save()
        messages.add_message(request, messages.INFO, "User has been successfully registered")

    return render_to_response("base_create_user_form.html",
                                           {'page_name': "Register new user"},
                                           context_instance=RequestContext(request))


def login_view(request):
    if request.method == "POST":
        username = html_to_content(request.POST['login_user'])
        password = html_to_content(request.POST['pswd_user'])
        next_to = request.GET.get('next')

        user = authenticate(username=username, password=password)
        if user is None:
            messages.add_message(request, messages.INFO, "Wrong username or password")
            return render_to_response("login_form.html",
                                           {'page_name': "Login",
                                            'username': username},
                                           context_instance=RequestContext(request))
        if not user.is_active:
            messages.add_message(request, messages.INFO, "This user is not active. Please contact administrator")
            return render_to_response("login_form.html",
                                           {'page_name': "Login",
                                            username: username},
                                           context_instance=RequestContext(request))
        login(request, user)
        if next_to is not None:
            return HttpResponseRedirect(next_to)
        else:
            return HttpResponseRedirect('/')
    else:
        return render_to_response("login_form.html",
                                           {'page_name': "Login"},
                                           context_instance=RequestContext(request))


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')
